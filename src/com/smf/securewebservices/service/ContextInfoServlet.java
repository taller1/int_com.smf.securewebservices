package com.smf.securewebservices.service;

import java.io.Writer;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.service.web.WebService;

import com.smf.securewebservices.utils.SecureWebServicesUtils;

public class ContextInfoServlet implements WebService {
	private static final String APPLICATION_JSON = "application/json";

  @Override
  public void doGet(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
	  JSONObject result = new JSONObject();
	  Client client = OBContext.getOBContext().getCurrentClient();
	  Role role = OBContext.getOBContext().getRole();
	  Organization org = OBContext.getOBContext().getCurrentOrganization();
	  User user  = OBContext.getOBContext().getUser();
	  Language lang  = OBContext.getOBContext().getLanguage();
	  Warehouse warehouse  = OBContext.getOBContext().getWarehouse();
	  String userLevel  = OBContext.getOBContext().getUserLevel();
	  String[] rclients  = OBContext.getOBContext().getReadableClients();
	  String[] rorgs  = OBContext.getOBContext().getReadableOrganizations();
	  Set<String> worgs  = OBContext.getOBContext().getWritableOrganizations();
	  JSONArray readableClients = new JSONArray();
	  JSONArray readableOrganizations = new JSONArray();
	  JSONArray writableOrganizations = new JSONArray();
	  
	  for (String rclient : rclients) {
		  readableClients.put(rclient);
	  }
	  for (String rorg : rorgs) {
		  readableOrganizations.put(rorg);
	  }
	  for (String worg : worgs) {
		  writableOrganizations.put(worg);
	  }
	  
	  result.put("client", client != null ? client.getId() : JSONObject.NULL);
	  result.put("role", role != null ? role.getId() : JSONObject.NULL);
	  result.put("organization", org != null ? org.getId() : JSONObject.NULL);
	  
	  List<Organization> childrenOrgs = SecureWebServicesUtils.getChildrenOrganizations(org);
	  JSONArray jsonChildrenOrgs = new JSONArray();
	  for (Organization organization : childrenOrgs) {
		  jsonChildrenOrgs.put(organization.getId());
	  }
	  result.put("childrenOrganization", jsonChildrenOrgs);

	  result.put("user", user != null ? user.getId() : JSONObject.NULL);
	  result.put("language", lang != null ? lang.getLanguage() : JSONObject.NULL);
	  result.put("warehouse", warehouse != null ? warehouse.getId() : JSONObject.NULL);
	  result.put("readableClients", readableClients);
	  result.put("readableOrganizations", readableOrganizations);
	  result.put("writableOrganizations", writableOrganizations);
	  result.put("userLevel", userLevel);

	  
	  
	  response.setContentType(APPLICATION_JSON);
		Writer out = response.getWriter();
		out.write(result.toString());
		out.close();
  }

  @Override
  public void doPost(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    doGet(path, request, response);
  }

  @Override
  public void doDelete(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
  }

  @Override
  public void doPut(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
  }

}
