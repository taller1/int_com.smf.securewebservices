package com.smf.securewebservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.ObjectNotFoundException;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.service.json.DataResolvingMode;
import org.openbravo.service.json.DataToJsonConverter;
import org.openbravo.service.json.JsonConstants;

public class SecureDataToJson extends DataToJsonConverter {
	private static final Logger log = LogManager.getLogger(SecureDataToJson.class);

	private Set<String> selectedProperties = new HashSet<>();
	private String displayProperty = null;
	private Boolean includeChildren = false;
	private List<String> additionalProperties = new ArrayList<String>();

	public List<JSONObject> toJsonObjects(List<? extends BaseOBObject> bobs, DataResolvingMode resolvingMode) {
		final List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		for (BaseOBObject bob : bobs) {
			jsonObjects.add(toJsonObject(bob, resolvingMode));
		}
		return jsonObjects;
	}

	@Override
	public JSONObject toJsonObject(BaseOBObject bob, DataResolvingMode dataResolvingMode) {
		try {
			final JSONObject jsonObject = new JSONObject();
			jsonObject.put(JsonConstants.IDENTIFIER, bob.getIdentifier());
			jsonObject.put(JsonConstants.ENTITYNAME, bob.getEntityName());
			jsonObject.put(JsonConstants.REF, encodeReference(bob));
			if (dataResolvingMode == DataResolvingMode.SHORT) {
				jsonObject.put(JsonConstants.ID, bob.getId());
				if (bob instanceof ActiveEnabled) {
					jsonObject.put(JsonConstants.ACTIVE, ((ActiveEnabled) bob).isActive());
				}
				return jsonObject;
			}
			final boolean isDerivedReadable = OBContext.getOBContext().getEntityAccessChecker()
					.isDerivedReadable(bob.getEntity());

			for (Property property : bob.getEntity().getProperties()) {
				if (property.isOneToMany()) {
					if (includeChildren) {
						try {
							@SuppressWarnings("unchecked")
							List<BaseOBObject> listResult = (List<BaseOBObject>) bob.get(property.getName());
							List<JSONObject> jsonList = toJsonObjects(listResult);
							JSONArray jsonArray = new JSONArray();
							for (JSONObject jsonObj : jsonList) {
								jsonArray.put(jsonObj);
							}
							jsonObject.put(property.getName(), jsonArray);
						} catch (Exception e) {
							log.error("Error al añadir la prodiedad: " + e.getMessage());
							jsonObject.put(property.getName(), JSONObject.NULL);
						}
					}
					continue;
				}
				// do not convert if the object is derived readable and the property is not
				if (isDerivedReadable && !property.allowDerivedRead()) {
					continue;
				}

				// check if the property is part of the selection
				if (selectedProperties.size() > 0 && !selectedProperties.contains(property.getName())) {
					continue;
				}

				Object value;
				if (dataResolvingMode == DataResolvingMode.FULL_TRANSLATABLE) {
					value = bob.get(property.getName(), OBContext.getOBContext().getLanguage());
				} else {
					value = bob.get(property.getName());
				}
				if (value != null) {
					if (property.isPrimitive()) {
						// TODO: format!
						jsonObject.put(property.getName(), convertPrimitiveValue(property, value));
					} else {
						addBaseOBObject(jsonObject, property, property.getName(), property.getReferencedProperty(),
								(BaseOBObject) value);
					}
				} else {
					jsonObject.put(property.getName(), JSONObject.NULL);
				}
			}
			for (String additionalProperty : additionalProperties) {
				// sometimes empty strings are passed in
				if (additionalProperty.length() == 0) {
					continue;
				}
				final Object value = DalUtil.getValueFromPath(bob, additionalProperty);
				if (value == null) {
					jsonObject.put(replaceDots(additionalProperty), (Object) null);
				} else if (value instanceof BaseOBObject) {
					final Property additonalPropertyObject = DalUtil.getPropertyFromPath(bob.getEntity(),
							additionalProperty);
					addBaseOBObject(jsonObject, additonalPropertyObject, additionalProperty,
							additonalPropertyObject.getReferencedProperty(), (BaseOBObject) value);
				} else {
					final Property property = DalUtil.getPropertyFromPath(bob.getEntity(), additionalProperty);
					// identifier
					if (additionalProperty.endsWith(JsonConstants.IDENTIFIER)) {
						jsonObject.put(replaceDots(additionalProperty), value);
					} else {
						jsonObject.put(replaceDots(additionalProperty), convertPrimitiveValue(property, value));
					}
				}
			}
			// When table references are set, the identifier should contain the display
			// property for as it
			// is done in the grid data. Refer
			// https://issues.openbravo.com/view.php?id=26696
			if (StringUtils.isNotEmpty(displayProperty)) {
				if (jsonObject.has(displayProperty + DalUtil.FIELDSEPARATOR + JsonConstants.IDENTIFIER)
						&& !jsonObject.get(displayProperty + DalUtil.FIELDSEPARATOR + JsonConstants.IDENTIFIER)
								.equals(JSONObject.NULL)) {
					jsonObject.put(JsonConstants.IDENTIFIER,
							jsonObject.get(displayProperty + DalUtil.FIELDSEPARATOR + JsonConstants.IDENTIFIER));
				} else if (jsonObject.has(displayProperty)
						&& !jsonObject.get(displayProperty).equals(JSONObject.NULL)) {
					jsonObject.put(JsonConstants.IDENTIFIER, jsonObject.get(displayProperty));
				}
			}

			// The recordTime is also added. This is the time (in milliseconds) at which the
			// record was
			// generated. This time can be used in the client side to compute the record
			// "age", for
			// example, or how much time has passed since the record was updated
			jsonObject.put("recordTime", new Date().getTime());
			return jsonObject;
		} catch (JSONException e) {
			throw new IllegalStateException(e);
		}
	}

	private void addBaseOBObject(JSONObject jsonObject, Property referencingProperty, String propertyName,
			Property referencedProperty, BaseOBObject obObject) throws JSONException {
		String identifier = null;
		// jsonObject.put(propertyName, toJsonObject(obObject,
		// DataResolvingMode.SHORT));
		if (referencedProperty != null) {
			try {
				jsonObject.put(propertyName.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR),
						obObject.get(referencedProperty.getName()));
			} catch (ObjectNotFoundException e) {
				// Referenced object does not exist, set UUID
				jsonObject.put(propertyName, e.getIdentifier());
				jsonObject.put(propertyName + DalUtil.FIELDSEPARATOR + JsonConstants.IDENTIFIER, e.getIdentifier());
				return;
			}
		} else {
			jsonObject.put(propertyName, obObject.getId());
		}
		// jsonObject.put(propertyName + DalUtil.DOT + JsonConstants.ID,
		// obObject.getId());

		if (referencingProperty != null && referencingProperty.hasDisplayColumn()) {

			Property displayColumnProperty = DalUtil.getPropertyFromPath(referencedProperty.getEntity(),
					referencingProperty.getDisplayPropertyName());
			// translating the displayPropertyName before retrieving inside the condition
			// statements. The values will be automatically translated if
			// getIdentifier() is called.
			if (referencingProperty.hasDisplayColumn()) {
				Object referenceObject = obObject.get(referencingProperty.getDisplayPropertyName(),
						OBContext.getOBContext().getLanguage(), (String) obObject.getId());
				if (referenceObject instanceof BaseOBObject) {
					identifier = referenceObject != null ? ((BaseOBObject) referenceObject).getIdentifier() : "";
				} else {
					identifier = referenceObject != null ? referenceObject.toString() : "";
				}
				if (referencingProperty.isDisplayValue()) {
					if (obObject.getEntity().hasProperty("searchKey")) {
						Object valueObject = obObject.get("searchKey", OBContext.getOBContext().getLanguage(),
								(String) obObject.getId());
						if (valueObject != null) {
							identifier = valueObject.toString() + " - " + identifier;
						} else {
							identifier = " - " + identifier;
						}
					} else {
						log.warn("Entity " + obObject.getEntity().getName()
								+ " does not have a searchKey property, the flag Displayed Value should not be used");
					}
				}
				// Allowing one level deep of displayed column pointing to references with
				// display column
				jsonObject.put(propertyName.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR) + DalUtil.FIELDSEPARATOR
						+ JsonConstants.IDENTIFIER, identifier);
			} else if (!displayColumnProperty.isPrimitive()) {
				// Displaying identifier for non primitive properties
				jsonObject.put(
						propertyName.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR) + DalUtil.FIELDSEPARATOR
								+ JsonConstants.IDENTIFIER,
						((BaseOBObject) obObject.get(referencingProperty.getDisplayPropertyName())).getIdentifier());
			} else {
				Object referenceObject = obObject.get(referencingProperty.getDisplayPropertyName(),
						OBContext.getOBContext().getLanguage(), (String) obObject.getId());
				identifier = referenceObject != null ? referenceObject.toString() : "";
				jsonObject.put(propertyName.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR) + DalUtil.FIELDSEPARATOR
						+ JsonConstants.IDENTIFIER, identifier);
			}
		} else {
			jsonObject.put(propertyName.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR) + DalUtil.FIELDSEPARATOR
					+ JsonConstants.IDENTIFIER, obObject.getIdentifier());
		}
	}

	public void setIncludeChildren(Boolean includeChildrenValue) {
		includeChildren = includeChildrenValue;
	}

	private String replaceDots(String value) {
		return value.replace(DalUtil.DOT, DalUtil.FIELDSEPARATOR);
	}
}